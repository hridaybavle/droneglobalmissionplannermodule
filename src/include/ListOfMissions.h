
#ifndef _LIST_OF_MISSIONS_H_
#define _LIST_OF_MISSIONS_H_

#include <numeric>
#include "droneMsgsROS/droneMission.h"
#include "droneMsgsROS/droneTask.h"
using namespace std;

#define MAX_MISSIONS 10

class ListOfMissions
{
private:
    std::vector<droneMsgsROS::droneMission> missions;
    std::vector<int> mission_task_id;
    std::vector<int> mission_task_id_offset;
public:
    ListOfMissions(void);
    ~ListOfMissions(void);
    void Delete(int index);
    void DeleteLastMission();
    void DestroyContent();
    bool Add(droneMsgsROS::droneMission mission_msg);
    void PrintListOfMissions();

    int getCurrentMissionTaskId();
    inline int getNumberOfMissions(){return missions.size();}
    droneMsgsROS::droneMission recoverLastMission();
    droneMsgsROS::droneMission getCurrentMission();
    void setCurrentMissionTaskId(int task_id);
    void setCurrentMissionTaskIdOffset(int offset);
    void IncrementCurrentMissionCounter();
};

#endif
