#include <stdio.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <cstdlib>
#include <vector>
#include <algorithm>
#include <utility>
#include <string>
#include "pugixml.hpp"
#include <Eigen/Dense>
#define _USE_MATH_DEFINES
#include <cmath>

//ROS
#include "ros/ros.h"
#include "std_srvs/Empty.h"
#include "std_msgs/Bool.h"
#include "std_msgs/Int32.h"
#include "std_msgs/Int16.h"
#include "std_msgs/UInt16.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Polygon.h"
#include "opencv_apps/Rect.h"

//Opencv
#include "opencv2/opencv.hpp"


//ROSModule and ROSMsgs
#include "droneMsgsROS/battery.h"
#include "droneMsgsROS/droneStatus.h"
#include "droneModuleROS.h"
#include "droneMsgsROS/droneMissionPlannerCommand.h"
#include "droneMsgsROS/points3DStamped.h"
#include "droneMsgsROS/vector3f.h"
#include "droneMsgsROS/droneTask.h"
#include "droneMsgsROS/droneMission.h"
#include "droneMsgsROS/droneNavData.h"
#include "droneMsgsROS/societyPose.h"
#include "droneMsgsROS/droneInfo.h"
#include "droneMsgsROS/askForModule.h"
#include "droneMsgsROS/visualObjectRecognized.h"
#include "droneMsgsROS/vectorVisualObjectsRecognized.h"
#include "droneMsgsROS/dronePerceptionManagerMissionRequest.h"
#include "droneMsgsROS/perceptionManagerUpdateMissionStateSrv.h"

#include "ListOfMissions.h"

//Aerostack
#include "communication_definition.h"



#define FLIGHT_HEIGHT 1.2
#define MAX_UAVS 10

class droneGMPROSModule : public Module
{
private:
    enum MissionType
    {
       EXPLORE,
       FIND_OBJECT,
       RESCUE,
       EXPLORE_WITH_NAVIGATION,
       PICK_OBJECT,
       RELEASE_OBJECT,
    };
    enum TaskType
    {
        Takeoff,
        Hover,
        MoveToPoint,
        MoveInVisualServoing,
        TurnInYaw,
        Sleep,
        GoHome,
        Flip,
        Land,
        LandAutonomously,
    };
    int num_UAVs;
    int drone_id_offset;
    int target_found_option;
    int target_found = true;
    MissionType mission_type;
    std::string configFile;
    float area_width, area_height, area_left_corner_x, area_left_corner_y;
    int num_centroids, num_sampling_points;
    bool generate_automatic_mission;
    cv::Point3f point_object_not_found;

    ListOfMissions* swarm_mission_list[MAX_UAVS];
    std::vector<cv::Point3f> region_centroids;
    std::vector<cv::Point3f> takeoff_UAVs_points;
    std::vector<std::vector<cv::Point3f> > missions_points;
    std::vector<std::vector<std::string> > missions_tasks_names;
    std::vector<droneMsgsROS::droneMission> swarm_missions_msg;
    std::vector<int> swarm_UAVs_current_task_id;


    //droneMsgsROS::droneTask drone_task_msg;
    ros::NodeHandle   nh;
    droneMsgsROS::points3DStamped UAV_mission_points_msg;
    droneMsgsROS::vector3f UAV_task_msg;
    opencv_apps::Rect area_info_msg;
    std::vector<int> swarm_ids;

    //ROS Publishers
    ros::Publisher droneMPcommandPub;
    std::vector<ros::Publisher> swarmGMP_missions_publ;
    std::vector<ros::Publisher> swarmGMP_break_mission_publ;
    std::vector<ros::Publisher> swarmGMP_area_info_publ;
    std::vector<ros::Publisher> swarmGMP_PM_update_mission_state_publ;
    std::vector<ros::ServiceClient> swarmGMP_start_submission_planner_client;
    std::vector<ros::ServiceClient> swarmGMP_PM_update_mission_state_client;
    ros::Publisher mission_points_publ;
    ros::Publisher missions_publ;

    //ROS Subscribers
    std::vector<ros::Subscriber> swarmGMP_subs;
    std::vector<ros::Subscriber> swarmGMP_battery_subs;
    std::vector<ros::Subscriber> swarmGMP_UAV_task_id_subs;
    std::vector<ros::Subscriber> swarmGMP_VS_measurement_not_found_subs;
    std::vector<ros::Subscriber> swarmGMP_objective_mission_ack_subs;
    std::vector<ros::Subscriber> swarmGMP_object_recognized_subs;
    std::vector<ros::Subscriber> swarmGMP_PM_mission_request_subs;
    ros::Subscriber societyBroadcastSubs;

    std_srvs::Empty emptyService;
    ros::ServiceClient startSubmissionPlannerClientSrv;
    ros::ServiceClient PM_update_mission_state_clientClientSrv;


public:
  droneGMPROSModule();
  ~droneGMPROSModule();
  void init();
  void random_init(float region_width, float region_height);
  void init_UAV_takeoff_points();
  void close();
  bool readConfigs(std::string configFile);

  bool resetValues();
  bool startVal();
  bool stopVal();

  bool run();
  void open(ros::NodeHandle & nIn, std::string ModuleName);

  void startGlobalMissionPlannerCallback(std_srvs::Empty::Request& request, std_srvs::Empty::Response& response);
  //void subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg);
  void subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg, const std::string& topic);
  void societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg);
  void batteryCallback(const droneMsgsROS::battery::ConstPtr& msg, const std::string &topic);
  void UAVTaskIdCallback(const std_msgs::Int16::ConstPtr& msg, const std::string &topic);
  void VSReferenceNotFoundCallback(const droneMsgsROS::vector3f::ConstPtr &msg, const std::string &topic);
  void subMissionPlannerTargetFoundAckCallback(const std_msgs::Int16::ConstPtr &msg, const std::string &topic);
  void PerceptionManagerMissionRequestCallback(const droneMsgsROS::dronePerceptionManagerMissionRequest::ConstPtr &msg, const std::string &topic);


  void setRegionCentroids(std::vector<cv::Point3f> centroids);
  std::vector<cv::Point3f> transformCentroids(std::vector<cv::Point3f> centroids);
  cv::Mat getPointsForSamplingArea(int num_points, float region_width, float region_height);
  std::vector<cv::Point3f> computeRegionCentroids(cv::Mat &SamplingPoints, int num_clusters);
  std::vector<std::vector<cv::Point3f> >  assignPointsToUAVs();
  std::vector<std::vector<cv::Point3f> >  sortMissionPoints(std::vector<std::vector<cv::Point3f> >& mission_points);
  void sendMissionPointsToMissionPlanner();
  std::vector<std::vector<cv::Point3f> > getMissionsPoints(){return missions_points;}
  void sendBroadcastAreaInfoToSubmissionPlanner();
  void sendBroadcastMissionToSubmissionPlanner();
  void sendBroadcastBreakMissionToSubmissionPlanner();
  void sendMissionToSubmissionPlanner(droneMsgsROS::droneMission mission, unsigned int UAV_id);
  void sendBreakMissionToSubmissionPlanner(unsigned int UAV_id);

  void generateBroadcastMissions();
  droneMsgsROS::droneMission generateMission(std::vector<std::string> &mission_tasks_names, std::vector<cv::Point3f> &UAV_mission_points, unsigned int UAV_id);
  void generateMissionsTasksNames();
  std::vector<std::string> getTaskModuleNames(std::string &task_name);
  std::vector<std::vector<std::string> > getMissionsTasksNames(){return missions_tasks_names;}
  inline float getAreaWidth(){return area_width;}
  inline float getAreaHeight(){return area_height;}
  inline float getAreaLeftCorner_x(){return area_left_corner_x;}
  inline float getAreaLeftCorner_y(){return area_left_corner_y;}
  inline int getNumCentroids(){return num_centroids;}
  inline int getNumSamplingPoints(){return num_sampling_points;}
  inline int getNumUAVs(){return num_UAVs;}

};
