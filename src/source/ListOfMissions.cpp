#include "ListOfMissions.h"


ListOfMissions::ListOfMissions(void)
{
    missions.clear();
    mission_task_id.clear();
    mission_task_id_offset.clear();
}


ListOfMissions::~ListOfMissions(void)
{
    DestroyContent();
}

bool ListOfMissions::Add(droneMsgsROS::droneMission mission_msg)
{
    cout<<"Adding MISSION..."<<endl;
    if(missions.size() < MAX_MISSIONS)
    {
        missions.push_back(mission_msg);
        mission_task_id.push_back(0);
        mission_task_id_offset.push_back(0);
        PrintListOfMissions();
        return true;
    }
    else
        return false;

}


void ListOfMissions::Delete(int index)
{
    if(index<0 || index>=missions.size())
        return;
    else
    {
        missions.erase(missions.begin() + index);
        mission_task_id.erase(mission_task_id.begin() + index);
        mission_task_id_offset.erase(mission_task_id_offset.begin() + index);
    }
}

void ListOfMissions::DeleteLastMission()
{
    if(missions.size())
    {
        missions.pop_back();
        mission_task_id.pop_back();
        mission_task_id_offset.pop_back();
    }
}

int ListOfMissions::getCurrentMissionTaskId()
{
    int index = missions.size()-1;
    return mission_task_id[index];
}


void ListOfMissions::setCurrentMissionTaskId(int task_id)
{
    if(task_id == -1 || missions.size() <= 0)
        return;
    int index = missions.size()-1;
    mission_task_id[index] = mission_task_id_offset[index] + task_id;
    cout<<"Current Mission id: "<<index<<" ; "<<"Current Task id: "<<mission_task_id[index]<<endl;
}

void ListOfMissions::setCurrentMissionTaskIdOffset(int offset)
{
    if(missions.size() <= 0)
        return;
    int index = missions.size()-1;
    mission_task_id_offset[index] = offset;
}

void ListOfMissions::IncrementCurrentMissionCounter()
{
    if(missions.size() <= 0)
        return;
    int index = missions.size()-1;
    int new_counter = mission_task_id[index] + 1;
    cout<<"Mission id: "<<index<<" ; "<<"New Counter: "<<new_counter<<endl;
    if(new_counter < missions[index].mission.size()-1)
        mission_task_id[index] = new_counter;
}

droneMsgsROS::droneMission ListOfMissions::recoverLastMission()
{
    int index = missions.size()-1;
    droneMsgsROS::droneMission new_mission;
    for(unsigned int i=mission_task_id[index];i<missions[index].mission.size();i++)
        new_mission.mission.push_back(missions[index].mission[i]);

    return new_mission;
}

droneMsgsROS::droneMission ListOfMissions::getCurrentMission()
{
    int index;
    if(missions.size()>0)
        index = missions.size()-1;
    else
        index = 0;
    return missions[index];
}

void ListOfMissions::PrintListOfMissions()
{
    std::cout<<"+++++ MISSION CONTAINER +++++"<<std::endl;
    for(unsigned i=0; i<missions.size();i++)
    {
        cout<<endl<<"--------- Mission "<<"["<<i<<"]"<<"---------"<<endl;
        cout<<missions[i]<<endl;
        cout<<"---------   ---------"<<endl;
    }
    cout<<"++++++++    ++++++++"<<endl<<endl;
}

void ListOfMissions::DestroyContent()
{
    missions.clear();
    mission_task_id.clear();
    mission_task_id_offset.clear();
}



