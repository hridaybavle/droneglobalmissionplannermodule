#include "droneGMPModule.h"
using namespace std;

droneGMPROSModule::droneGMPROSModule():
  Module(droneModule::active, 15.0)
{
//    drone_id_offset = 5;
//    num_UAVs = 1;
    point_object_not_found.x = 0.0;
    point_object_not_found.y = 0.0;
    point_object_not_found.z = 0.0;
    target_found = 0;
    target_found_option = 3;
    std::cout << "GlobalMissionPlannerROSModule(..), enter and exit" << std::endl;
    mission_type = MissionType::EXPLORE_WITH_NAVIGATION;
    return;
}

droneGMPROSModule::~droneGMPROSModule()
{
    for(unsigned i=0;i<num_UAVs;i++)
        delete swarm_mission_list[i];
    close();
    return;
}


void droneGMPROSModule::close()
{

    return;
}

bool droneGMPROSModule::resetValues()
{
    if(!Module::resetValues())
        return false;

    return true;
}

bool droneGMPROSModule::startVal()
{
  if(!Module::startVal())
    return false;

  for(int i=0;i<num_UAVs;i++)
  {
      //For starting the subMissionPlanner
      if(i > 0)
          ros::Duration(0).sleep();
      if(!swarmGMP_start_submission_planner_client[i].call(emptyService))
        return false;
  }

   return true;
}

bool droneGMPROSModule::stopVal()
{
    if(!Module::stopVal())
        return false;

     return true;
}

bool droneGMPROSModule::run()
{
  //TODO: Run the global mission planner algo here
  if(!Module::run())
    return false;


  return true;
}


void droneGMPROSModule::init()
{
    //readConfigs(stackPath + "configs/GMP/global_mission_planner_configFile.xml");

    while(!moduleStarted)
    {
        //Wait for the Service to Start
        ros::spinOnce();
    }

    int numOfUAVs = getNumUAVs();
    for(unsigned int i=0;i<numOfUAVs;i++)
    {
        swarm_UAVs_current_task_id.push_back(0);
        swarm_mission_list[i] = new ListOfMissions;
        swarm_mission_list[i]->Add(swarm_missions_msg[i]);
    }

    float region_width = getAreaWidth();
    float region_height = getAreaHeight();
    float region_left_corner_x = getAreaLeftCorner_x();
    float region_left_corner_y = getAreaLeftCorner_y();
    area_info_msg.width = region_width;
    area_info_msg.height = region_height;
    area_info_msg.x = region_left_corner_x;
    area_info_msg.y = region_left_corner_y;


    if(generate_automatic_mission)
    {
        int num_centroids = getNumCentroids();
        int num_sampling_ponts = getNumSamplingPoints();
        cv::Mat RandomPoints = getPointsForSamplingArea(num_sampling_ponts, region_width, region_height);
        //cout<<"Random Points Generated:"<<endl;
        //cout<<RandomPoints<<endl;

        std::vector<cv::Point3f> Centroids = computeRegionCentroids(RandomPoints, num_centroids);
        std::vector<cv::Point3f> Centroids_transformed = transformCentroids(Centroids);
        setRegionCentroids(Centroids_transformed);
        cout<<endl<<"Centroids:"<<endl;
        for(int i=0;i<Centroids.size();i++)
            cout<<"x: "<<Centroids[i].x<<" ; "<<"y: "<<Centroids[i].y<<" ; "<<"z: "<<Centroids[i].z<<endl;
        cout<<endl<<endl;

        cout<<endl<<"Centroids Transformed:"<<endl;
        for(int i=0;i<Centroids_transformed.size();i++)
            cout<<"x: "<<Centroids_transformed[i].x<<" ; "<<"y: "<<Centroids_transformed[i].y<<" ; "
               <<"z: "<<Centroids_transformed[i].z<<endl;
        cout<<endl<<endl;

        assignPointsToUAVs();

        generateMissionsTasksNames();
        generateBroadcastMissions();
    }

    sendBroadcastMissionToSubmissionPlanner();
    sendBroadcastAreaInfoToSubmissionPlanner();

}

void droneGMPROSModule::init_UAV_takeoff_points()
{
    for(int i=0;i<num_UAVs;i++)
    {
        cv::Point3f r;
        //std::vector<std::string> task_name;
        switch(i)
        {

            case 0:
                r.x =  0;
                r.y =  0;
                r.z = FLIGHT_HEIGHT;
                break;
            case 1:
                r.x =  7.0;
                r.y =  1.0;
                r.z = FLIGHT_HEIGHT;
                break;
            case 2:
                r.x =  4.5;
                r.y =  8.0;
                r.z = FLIGHT_HEIGHT;
                break;

        }

        takeoff_UAVs_points.push_back(r);

    }

    cout<<"UAV Takeoff Positions:"<<endl;
    for(int i=0;i<takeoff_UAVs_points.size();i++)
        cout<<"x: "<<takeoff_UAVs_points[i].x<< " ; "<<"y: "<<takeoff_UAVs_points[i].y<< " ; "<<"z: "<<takeoff_UAVs_points[i].z<<endl;
    cout<<endl<<endl;
}

void droneGMPROSModule::random_init(float region_width, float region_height)
{
    static bool first = true;
    if ( first )
    {
       srand(time(NULL));
       first = false;
    }

    for(int i=0;i<num_UAVs;i++)
    {
        std::vector<std::string> task_name;
        cv::Point3f r;
        r.x =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_width));
        r.y =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_height));
        r.z = FLIGHT_HEIGHT;

        takeoff_UAVs_points.push_back(r);

        task_name.push_back("Takeoff");
        task_name.push_back("Hover");
        task_name.push_back("Land");
        missions_tasks_names.push_back(task_name);
    }

    cout<<"UAV Takeoff Positions:"<<endl;
    for(int i=0;i<takeoff_UAVs_points.size();i++)
        cout<<"x: "<<takeoff_UAVs_points[i].x<< " ; "<<"y: "<<takeoff_UAVs_points[i].y<< " ; "<<"z: "<<takeoff_UAVs_points[i].z<<endl;
    cout<<endl<<endl;

}

bool droneGMPROSModule::readConfigs(string configFile)
{
    pugi::xml_document doc;

    std::ifstream nameFile(configFile.c_str());
    pugi::xml_parse_result result = doc.load(nameFile);

    if(!result)
    {
        cout << "ERROR: Could not load the file: " << result.description() << endl;
        return 0;
    }


    pugi::xml_node Configuration = doc.child("Global_Mission_Planner_config");
    std::string readingValue;


    pugi::xml_node nodeArea = Configuration.child("explorationArea");


    cout<<endl<<"+++++ Global Mission Planner Config File +++++"<<endl;
    readingValue = nodeArea.child("left_corner").child_value("x");
    area_left_corner_x = atof(readingValue.c_str());
    readingValue = nodeArea.child("left_corner").child_value("y");
    area_left_corner_y = atof(readingValue.c_str());
    readingValue = nodeArea.child_value("width");
    area_width = atof(readingValue.c_str());
    readingValue = nodeArea.child_value("height");
    area_height = atof(readingValue.c_str());
    readingValue = nodeArea.child_value("num_centroids");
    num_centroids = (int)atoi(readingValue.c_str());
    readingValue = nodeArea.child_value("num_sampling_points");
    num_sampling_points = (int)atoi(readingValue.c_str());

    cout<<"Left Corner x: "<<area_left_corner_x<<endl;
    cout<<"Left Corner y: "<<area_left_corner_y<<endl;
    cout<<"Area Width: "<<area_width<<endl;
    cout<<"Area Height: "<<area_height<<endl;
    cout<<"Num Voronoi Centroids: "<<num_centroids<<endl;
    cout<<"Num Sampling points: "<<num_sampling_points<<endl;
    cout<<endl<<endl;

    readingValue = Configuration.child_value("generate_automatic_mission");
    generate_automatic_mission = (bool)atoi(readingValue.c_str());
    cout<<"Generate Automatic Mission Flag: "<<generate_automatic_mission<<endl;


    int numOfUAVs = 0;
    missions_points.clear();
    missions_tasks_names.clear();

    pugi::xml_node nodeSwarm = Configuration.child("SWARM_configuration");
    cout<<"***** UAV PARAMETERS *****"<<endl;
    for(pugi::xml_node nodeUAV = nodeSwarm.first_child(); nodeUAV; nodeUAV = nodeUAV.next_sibling())
    {
        readingValue = nodeUAV.child_value("drone_id");
        int id_offset = (int)atoi(readingValue.c_str());
        if(numOfUAVs == 0)
            drone_id_offset = id_offset;
        cout<<"drone_id: "<<id_offset<<endl;
        cout<<"drone_id_offset: "<<drone_id_offset<<endl;
        cv::Point3f UAV_takeoff_point;
        readingValue = nodeUAV.child("takeoff_point").child_value("x");
        UAV_takeoff_point.x = atof(readingValue.c_str());
        readingValue = nodeUAV.child("takeoff_point").child_value("y");
        UAV_takeoff_point.y = atof(readingValue.c_str());
        readingValue = nodeUAV.child("takeoff_point").child_value("z");
        UAV_takeoff_point.z = atof(readingValue.c_str());
        takeoff_UAVs_points.push_back(UAV_takeoff_point);
        //cout<<"UAV Takeoff Point: "<<UAV_takeoff_point<<endl;
        cout<<endl;


        std::vector<std::string> tasks_names;
        std::vector<cv::Point3f> vec_points;
        droneMsgsROS::droneMission drone_mission_msg;
        drone_mission_msg.header.stamp = ros::Time::now();
        cout<<"+++ UAV MISSION +++"<<endl;
        pugi::xml_node nodeSwarm = nodeUAV.child("Mission");
        for(pugi::xml_node nodeTask = nodeSwarm.first_child(); nodeTask; nodeTask = nodeTask.next_sibling())
        {
            droneMsgsROS::droneTask drone_task_msg;
            cout<<endl<<"-- UAV TASK --"<<endl;
            readingValue = nodeTask.child_value("name");
            std::string task_name = readingValue.c_str();
            readingValue = nodeTask.child_value("time");
            drone_task_msg.time = atoi(readingValue.c_str());
            readingValue = nodeTask.child_value("mpCommand");
            drone_task_msg.mpCommand = atoi(readingValue.c_str());
            readingValue = nodeTask.child_value("yaw");
            drone_task_msg.yaw = atof(readingValue.c_str());
            readingValue = nodeTask.child_value("yawSelector");
            drone_task_msg.yawSelector = atoi(readingValue.c_str());
            cv::Point3f point;
            readingValue = nodeTask.child("Point").child_value("x");
            drone_task_msg.point.x = atof(readingValue.c_str());
            readingValue = nodeTask.child("Point").child_value("y");
            drone_task_msg.point.y = atof(readingValue.c_str());
            readingValue = nodeTask.child("Point").child_value("z");
            drone_task_msg.point.z = atof(readingValue.c_str());
            drone_task_msg.module_names = getTaskModuleNames(task_name);
            cout<<"task_name: "<<task_name<<endl;
            cout<<"Task Information: "<<endl<<drone_task_msg<<endl;

            tasks_names.push_back(task_name);
            vec_points.push_back(point);
            drone_mission_msg.mission.push_back(drone_task_msg);
        }
        if(!generate_automatic_mission)
        {
            missions_tasks_names.push_back(tasks_names);
            swarm_missions_msg.push_back(drone_mission_msg);
        }
        missions_points.push_back(vec_points);
        numOfUAVs++;
    }

    cout<<"*** Missions Tasks Names from Config File ***"<<endl;
    for(int i=0;i<missions_tasks_names.size();i++)
    {
        for(int j=0;j<missions_tasks_names[i].size();j++)
            cout<<missions_tasks_names[i][j]<<endl;
        cout<<endl<<endl;
    }


    cout<<"UAV Takeoff Positions:"<<endl;
    for(int i=0;i<takeoff_UAVs_points.size();i++)
        cout<<"x: "<<takeoff_UAVs_points[i].x<< " ; "<<"y: "<<takeoff_UAVs_points[i].y<< " ; "<<"z: "<<takeoff_UAVs_points[i].z<<endl;
    cout<<endl<<endl;
    num_UAVs = numOfUAVs;
    cout<<"num_UAVs: "<<num_UAVs<<endl;


    return true;
}


void droneGMPROSModule::open(ros::NodeHandle &nIn, std::string ModuleName)
{
    nh = nIn;

    Module::open(nh,ModuleName);

    ros::param::get("~config_file", configFile);
    if ( configFile.length() == 0)
    {
        configFile="global_mission_planner_configFile.xml";
    }
    readConfigs(stackPath + "configs/GMP/" + configFile);


    //TODO: publish and subscribe all the ROS topics here
    //mission_points_publ = nh.advertise<droneMsgsROS::droneMission>("globalMissionPlannerMission", 1, true);
    societyBroadcastSubs = n.subscribe("societyBroadcast", 100, &droneGMPROSModule::societyBroadcastCallback, this);


    missions_publ = nh.advertise<droneMsgsROS::droneMission>("globalMissionPlannerMission", 1, true);
    //startSubmissionPlannerClientSrv = nh.serviceClient<std_srvs::Empty>("startDroneSubmissionPlanner");


    for(int i=drone_id_offset;i<(num_UAVs+drone_id_offset);i++)
    {
        std::string topic_PM_update_mission_state_name = std::string("/drone")+std::to_string(i)+"/"+"perception_manager/update_mission_state";
        std::string topic_PM_mission_request_name = std::string("/drone")+std::to_string(i)+"/"+"perception_manager/mission_request";
        std::string topic_publ_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerMission";
        std::string topic_area_info_publ_name = std::string("/drone")+std::to_string(i)+"/"+"area_info";
        std::string topic_subs_name = std::string("/drone")+std::to_string(i)+"/"+"subMissionPlannerState";
        std::string topic_battery_subs_name = std::string("/drone")+std::to_string(i)+"/"+"battery";
        std::string topic_UAV_task_id_subs_name = std::string("/drone")+std::to_string(i)+"/"+"UAV_task_id";
        std::string topic_VS_measurement_not_found_subs_name = std::string("/drone")+std::to_string(i)+"/"+"VS_measurement_not_found";
        std::string topic_target_found_ack_subs_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerTargetFoundAck";
        //std::string topic_object_recognized_subs_name = std::string("/drone")+std::to_string(i)+"/"+"agentMissionPlannerObjectRecognized";
        std::string topic_break_mission_name = std::string("/drone")+std::to_string(i)+"/"+"globalMissionPlannerBreakMissionAck";
        std::string start_submission_planner_client = std::string("/drone")+std::to_string(i)+"/"+"droneSubMissionPlanner/start";
        std::string PM_update_mission_state_client = std::string("/drone")+std::to_string(i)+"/"+"perception_manager/update_mission_state_srv";


        ros::Publisher swarm_PM_update_mission_state_publ = nh.advertise<std_msgs::Int32>(topic_PM_update_mission_state_name, 1, true);
        ros::Publisher swarm_area_info_publ = nh.advertise<opencv_apps::Rect>(topic_area_info_publ_name, 1, true);
        ros::Publisher swarm_missions_publ = nh.advertise<droneMsgsROS::droneMission>(topic_publ_name, 1, true);
        ros::Publisher swarm_target_found_publ = nh.advertise<std_msgs::Bool>(topic_break_mission_name, 1, true);
        startSubmissionPlannerClientSrv = nh.serviceClient<std_srvs::Empty>(start_submission_planner_client,1);
        PM_update_mission_state_clientClientSrv = nh.serviceClient<droneMsgsROS::perceptionManagerUpdateMissionStateSrv>(PM_update_mission_state_client,1);

        swarmGMP_PM_update_mission_state_publ.push_back(swarm_PM_update_mission_state_publ);
        swarmGMP_area_info_publ.push_back(swarm_area_info_publ);
        swarmGMP_missions_publ.push_back(swarm_missions_publ);
        swarmGMP_break_mission_publ.push_back(swarm_target_found_publ);
        swarmGMP_start_submission_planner_client.push_back(startSubmissionPlannerClientSrv);
        swarmGMP_PM_update_mission_state_client.push_back(PM_update_mission_state_clientClientSrv);

        //ros::Subscriber subs = nh.subscribe(topic_subs_name, 100, &droneGMPROSModule::subMissionPlannerStateCallback, this);
        ros::Subscriber PM_mission_request_subs = nh.subscribe<droneMsgsROS::dronePerceptionManagerMissionRequest>(topic_PM_mission_request_name, 1, boost::bind(&droneGMPROSModule::PerceptionManagerMissionRequestCallback, this, _1, topic_PM_mission_request_name));
        ros::Subscriber subs = nh.subscribe<droneMsgsROS::droneStatus>(topic_subs_name, 1000, boost::bind(&droneGMPROSModule::subMissionPlannerStateCallback, this, _1, topic_subs_name));
        ros::Subscriber batterySubs = nh.subscribe<droneMsgsROS::battery>(topic_battery_subs_name, 1, boost::bind(&droneGMPROSModule::batteryCallback, this, _1, topic_battery_subs_name));
        ros::Subscriber VS_measurement_not_found_subs = nh.subscribe<droneMsgsROS::vector3f>(topic_VS_measurement_not_found_subs_name, 1, boost::bind(&droneGMPROSModule::VSReferenceNotFoundCallback, this, _1, topic_VS_measurement_not_found_subs_name));
        ros::Subscriber UAV_task_id_subs = nh.subscribe<std_msgs::Int16>(topic_UAV_task_id_subs_name, 1, boost::bind(&droneGMPROSModule::UAVTaskIdCallback, this, _1, topic_UAV_task_id_subs_name));
        ros::Subscriber subMissionPlanner_target_found_AckSubs = nh.subscribe<std_msgs::Int16>(topic_target_found_ack_subs_name, 1, boost::bind(&droneGMPROSModule::subMissionPlannerTargetFoundAckCallback, this, _1, topic_target_found_ack_subs_name));
        swarmGMP_subs.push_back(subs);
        swarmGMP_battery_subs.push_back(batterySubs);
        swarmGMP_UAV_task_id_subs.push_back(UAV_task_id_subs);
        swarmGMP_VS_measurement_not_found_subs.push_back(VS_measurement_not_found_subs);
        swarmGMP_objective_mission_ack_subs.push_back(subMissionPlanner_target_found_AckSubs);
        swarmGMP_PM_mission_request_subs.push_back(PM_mission_request_subs);

    }

    droneModuleOpened = true;

    return;
}

void droneGMPROSModule::societyBroadcastCallback(const std_msgs::Int32::ConstPtr &msg)
{

}


//void droneGMPROSModule::subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg)
void droneGMPROSModule::subMissionPlannerStateCallback(const droneMsgsROS::droneStatus::ConstPtr& msg, const std::string& topic)
{
//  const std::string& publisher_name = event.getPublisherName();
//  ROS_INFO("publisher: %s\ttopic: %s", publisher_name.c_str(), topic.c_str());
}

void droneGMPROSModule::batteryCallback(const droneMsgsROS::battery::ConstPtr& msg, const std::string& topic)
{
    //batteryMsgs=*msg;
    //ROS_INFO("battery topic: %s", topic.c_str());
    return;
}


void droneGMPROSModule::UAVTaskIdCallback(const std_msgs::Int16::ConstPtr &msg, const string &topic)
{
    int id_drone;
    for(unsigned int i=0;i<swarmGMP_UAV_task_id_subs.size();i++)
    {
        if(swarmGMP_UAV_task_id_subs[i].getTopic() == topic)
        {
            id_drone = i;
            break;
        }
    }

    swarm_mission_list[id_drone]->setCurrentMissionTaskId(msg->data);
    swarm_UAVs_current_task_id[id_drone] = msg->data;
    cout<<"++++ Current AMP TASK id +++"<<endl;
    for(unsigned int i=0;i<swarm_UAVs_current_task_id.size();i++)
        cout<<swarm_UAVs_current_task_id[i]<<" ; ";
    cout<<endl;


    //Re-Send Previous Unfinished Mission
    if(swarm_UAVs_current_task_id[id_drone] == -1)
    {
        droneMsgsROS::perceptionManagerUpdateMissionStateSrv srv;
        srv.request.mission_state = 2;
        swarmGMP_PM_update_mission_state_client[id_drone].call(srv);
        bool PM_update_mission_state_requested = srv.response.ack;

        //std_msgs::Int32 mission_update_msg;
        //swarmGMP_PM_update_mission_state_publ[id_drone].publish(mission_update_msg);
        swarm_mission_list[id_drone]->DeleteLastMission();
        if(swarm_mission_list[id_drone]->getNumberOfMissions() && (!PM_update_mission_state_requested))
        {
            droneMsgsROS::droneMission new_mission_from_list =  swarm_mission_list[id_drone]->recoverLastMission();
            cout<<endl<<endl<<"---- New Mission Generated by ListOfMissions ----"<<endl<<new_mission_from_list<<endl;
            sendMissionToSubmissionPlanner(new_mission_from_list, id_drone);
        }
    }

}

void droneGMPROSModule::VSReferenceNotFoundCallback(const droneMsgsROS::vector3f::ConstPtr &msg, const std::string &topic)
{
    int id_drone;
    for(unsigned int i=0;i<swarmGMP_VS_measurement_not_found_subs.size();i++)
    {
        if(swarmGMP_VS_measurement_not_found_subs[i].getTopic() == topic)
        {
            id_drone = i;
            break;
        }
    }

    float distance = 0.0;
    cv::Point3f msg_point;
    msg_point.x = msg->x;
    msg_point.y = msg->y;
    msg_point.z = msg->z;
    distance = cv::norm(point_object_not_found - msg_point);
    if(distance < 0.01)
        return;

    point_object_not_found.x = msg->x;
    point_object_not_found.y = msg->y;
    point_object_not_found.z = msg->z;

    cout<<"VS Measurement NOT Found!"<<endl;
    cout<<"In Point: "<<point_object_not_found<<endl;

    swarm_mission_list[id_drone]->DeleteLastMission();
    if(swarm_mission_list[id_drone]->getNumberOfMissions())
    {
        droneMsgsROS::droneMission new_mission_from_list =  swarm_mission_list[id_drone]->recoverLastMission();
        cout<<endl<<endl<<"---- New Mission Generated by ListOfMissions ----"<<endl<<new_mission_from_list<<endl;
        sendMissionToSubmissionPlanner(new_mission_from_list, id_drone);
    }
}

void droneGMPROSModule::PerceptionManagerMissionRequestCallback(const droneMsgsROS::dronePerceptionManagerMissionRequest::ConstPtr &msg, const string &topic)
{
    if(!moduleStarted)
        return;
    int id_drone;
    for(unsigned int i=0;i<swarmGMP_PM_mission_request_subs.size();i++)
    {
        if(swarmGMP_PM_mission_request_subs[i].getTopic() == topic)
        {
            id_drone = i;
            break;
        }
    }


    //Save the Current Mission
    int mission_id_offset = swarm_mission_list[id_drone]->getCurrentMissionTaskId();
    swarm_mission_list[id_drone]->setCurrentMissionTaskIdOffset(mission_id_offset);



    unsigned int mission_requested_type = msg->submission_type;
    std::vector<std::string> task_names;
    std::vector<cv::Point3f> points_to_go;
    cv::Point3f point_to_go;
    switch(mission_requested_type)
    {
        case droneMsgsROS::dronePerceptionManagerMissionRequest::PICK_OBJECT:
            //cout<<"Assigning PICK_OBJECT MISSION..."<<endl;
            point_to_go.x = msg->object_pose.position.x;
            point_to_go.y = msg->object_pose.position.y;
            point_to_go.z = msg->object_pose.position.z;
            cout<<"Point To Go: "<<point_to_go<<endl;
            break;
        case droneMsgsROS::dronePerceptionManagerMissionRequest::RELEASE_OBJECT:
//            missions_tasks_names.clear();
//            missions_points.clear();
            //cout<<"Assigning RELEASE_OBJECT MISSION..."<<endl;
            point_to_go.x = msg->object_pose.position.x;
            point_to_go.y = msg->object_pose.position.y;
            point_to_go.z = FLIGHT_HEIGHT;
            cout<<"Point To Go: "<<point_to_go<<endl;
            for(int i=0;i<num_UAVs;i++)
            {
                if(i == id_drone)
                {

                        task_names.push_back("MoveToPoint");
                        task_names.push_back("Sleep");
                        task_names.push_back("MoveInVisualServoingRl");
                        task_names.push_back("Sleep");
//                        task_names.push_back("GoHome");
//                        task_names.push_back("Land");

                        points_to_go.push_back(point_to_go);
                }

            }
            break;
    }

    if(points_to_go.size() && task_names.size())
    {
        droneMsgsROS::droneMission new_mission = generateMission(task_names, points_to_go, id_drone);
        swarm_mission_list[id_drone]->Add(new_mission);
        sendMissionToSubmissionPlanner(new_mission, id_drone);
    }

}

void droneGMPROSModule::subMissionPlannerTargetFoundAckCallback(const std_msgs::Int16::ConstPtr &msg, const std::string &topic)
{
    if(!moduleStarted)
        return;
    ROS_INFO("Target Found topic: %s", topic.c_str());
    int id_drone = msg->data - drone_id_offset;
    if(1)
    {
        target_found++;
        if(target_found > 1)
            return;
        missions_tasks_names.clear();
        missions_points.clear();
        if(mission_type == MissionType::FIND_OBJECT)
        {
            cout<<"Object Found!"<<endl;
            cout<<"Assigning NEW MISSION..."<<endl;
            for(int i=0;i<num_UAVs;i++)
            {
                std::vector<std::string> task_name;
                std::vector<cv::Point3f> takeoff_point;
                std::vector<cv::Point3f> altitude_point;
                switch(target_found_option)
                {
                    case 1:

                        task_name.push_back("GoHome");
                        task_name.push_back("Land");
                        missions_tasks_names.push_back(task_name);


                        takeoff_point.push_back(takeoff_UAVs_points[i]);
                        missions_points.push_back(takeoff_point);
                        break;

                    case 2:
                        if(i == id_drone)
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Flip");
                            task_name.push_back("Hover");
                            task_name.push_back("Land");
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Land");
                        }
                        missions_tasks_names.push_back(task_name);
                        break;

                    case 3:
                        if(i == id_drone)
                        {
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("Land");

                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("Land");

                            altitude_point.push_back(cv::Point3f(0,0,0.0));
                            altitude_point.push_back(cv::Point3f(0,0,0.0));
                        }
                        missions_points.push_back(altitude_point);
                        missions_tasks_names.push_back(task_name);
                        break;

                    case 4:
                        if(i == id_drone)
                        {
                            task_name.push_back("Hover");
//                            task_name.push_back("Flip");
//                            task_name.push_back("Hover");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("MoveToPoint");
                            task_name.push_back("GoHome");
                            task_name.push_back("Land");


                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(cv::Point3f(0,0,0.7));
                            altitude_point.push_back(cv::Point3f(0,0,1.5));
                            altitude_point.push_back(takeoff_UAVs_points[i]);
                        }
                        else
                        {
                            task_name.push_back("Hover");
                            task_name.push_back("GoHome");
                            task_name.push_back("Land");

                            altitude_point.push_back(takeoff_UAVs_points[i]);
                        }
                        missions_points.push_back(altitude_point);
                        missions_tasks_names.push_back(task_name);
                        break;

                }
            }

        }


//        cout<<"*** Missions Names ***"<<endl;
//        for(int i=0;i<missions_tasks_names.size();i++)
//        {
//            for(int j=0;j<missions_tasks_names[i].size();j++)
//                cout<<missions_tasks_names[i][j]<<endl;
//            cout<<endl<<endl;
//        }

//        cout<<"*** UAV_mission_points FINAL ***"<<endl;
//        for(int i=0;i<missions_points.size();i++)
//        {
//            for(int j=0;j<missions_points[i].size();j++)
//                cout<<missions_points[i][j]<<endl;
//            cout<<endl<<endl;
//        }

        generateBroadcastMissions();

        sendBroadcastBreakMissionToSubmissionPlanner();
        sendBroadcastMissionToSubmissionPlanner();

    }
}

cv::Mat droneGMPROSModule::getPointsForSamplingArea(int num_points, float region_width, float region_height)
{
    cv::Mat random_points(num_points,2,CV_32F);

    static bool first = true;
    if ( first )
    {
       srand(time(NULL));
       first = false;
    }

    for(int i=0;i<num_points;i++)
    {
        random_points.at<float>(i,0) =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_width));
        random_points.at<float>(i,1) =  static_cast <float> (rand()) / (static_cast <float> (RAND_MAX/region_height));
    }

    return random_points;

}

std::vector<cv::Point3f> droneGMPROSModule::computeRegionCentroids(cv::Mat &SamplingPoints, int num_clusters)
{
    //cv::Mat I = cv::Mat::zeros(300,200,CV_8U);
    cv::Mat Centroids;
    cv::Mat Labels;


    double compactness = 0.0;
    cv::TermCriteria criteria_kmeans(CV_TERMCRIT_EPS+CV_TERMCRIT_ITER, 50, 0.02);
    compactness = cv::kmeans(SamplingPoints, num_clusters, Labels, criteria_kmeans, 5, cv::KMEANS_RANDOM_CENTERS, Centroids);

    cout<<"Centroids Mat:"<<endl<<Centroids<<endl;
    for(int i=0;i<Centroids.rows;i++)
        region_centroids.push_back(cv::Point3f(Centroids.at<float>(i,0), Centroids.at<float>(i,1), FLIGHT_HEIGHT));

    return region_centroids;
}

void droneGMPROSModule::setRegionCentroids(std::vector<cv::Point3f> centroids)
{
    region_centroids.clear();
    region_centroids.insert(region_centroids.end(), centroids.begin(), centroids.end());
}

std::vector<cv::Point3f> droneGMPROSModule::transformCentroids(std::vector<cv::Point3f> centroids)
{
    Eigen::Matrix4d T_world;
    std::vector<cv::Point3f> region_centroids_transformed;
    T_world(0,0) = cos(-M_PI/2.0); T_world(0,1) = -sin(-M_PI/2.0); T_world(0,2) = 0.0; T_world(0,3) = getAreaLeftCorner_x();
    T_world(1,0) = sin(-M_PI/2.0); T_world(1,1) = cos(-M_PI/2.0); T_world(1,2) = 0.0; T_world(1,3) = getAreaLeftCorner_y();
    T_world(2,0) = 0.0; T_world(2,1) = 0.0; T_world(2,2) = 1.0; T_world(2,3) = 0.0;
    T_world(3,0) = 0.0; T_world(3,1) = 0.0; T_world(3,2) = 0.0; T_world(3,3) = 1.0;

    for(unsigned int i=0;i<centroids.size();i++)
    {
        Eigen::Vector4d centroid;
        Eigen::Vector4d new_centroid;
        cv::Point3f new_centroid_cv;
        centroid(0) = centroids[i].x;
        centroid(1) = centroids[i].y;
        centroid(2) = centroids[i].z;
        centroid(3) = 1.0;

        new_centroid = T_world * centroid;
        new_centroid_cv.x = new_centroid(0);
        new_centroid_cv.y = new_centroid(1);
        new_centroid_cv.z = new_centroid(2);
        region_centroids_transformed.push_back(new_centroid_cv);
    }

    return region_centroids_transformed;
}

std::vector<std::vector<cv::Point3f> > droneGMPROSModule::assignPointsToUAVs()
{
    std::vector<std::vector<cv::Point3f> > UAV_mission_points;
    std::vector<std::vector<cv::Point3f> > UAV_mission_points_v2;
    std::vector<float> distances_to_mission_points;
    std::vector<int> centroids_id;

    for(int i=0;i<region_centroids.size();i++)
    {
        std::vector<float> distances;
        for(int j=0;j<takeoff_UAVs_points.size();j++)
            distances.push_back(cv::norm(region_centroids[i] - takeoff_UAVs_points[j]));

//        for(int z=0;z<distances.size();z++)
//            cout<<distances[z]<<endl;


        distances_to_mission_points.push_back(*std::min_element(distances.begin(), distances.end()));
        centroids_id.push_back(std::min_element(distances.begin(), distances.end()) - distances.begin() + 1);
//        cout<<"Minimum element idx: "<<centroids_id[i]<<endl;
//        cout<<"Distance to Minimum element: "<<distances_to_mission_points[i]<<endl;
//        cout<<endl<<endl;


    }
    cout<<"Points Assigned!"<<endl;

    cout<<"ID de los Centroides:"<<endl;
    for(int i=0;i<centroids_id.size();i++)
        cout<<centroids_id[i]<<endl;

//    cout<<"distances_to_mission_points: "<<endl;
//    for(int i=0;i<distances_to_mission_points.size();i++)
//        cout<<distances_to_mission_points[i]<<endl;

    for(int i=0;i<num_UAVs;i++)
    {
        std::vector<cv::Point3f> p;
        std::vector<float> dist;
        for(int j=0;j<region_centroids.size();j++)
        {
            if(centroids_id[j] == (i+1))
            {
                p.push_back(region_centroids[j]);
                dist.push_back(distances_to_mission_points[j]);
            }
        }
        UAV_mission_points_v2.push_back(p);

        //Sort the vector p according to the distance
        std::vector<int> ind(dist.size());
        std::iota(ind.begin(), ind.end(), 0);
        auto comparator = [&dist](float a, float b){ return dist[a] < dist[b]; };
        std::sort(ind.begin(), ind.end(), comparator);

        std::vector<cv::Point3f> vCopy = p;
        for(int z = 0; z < ind.size(); ++z)
            p[z] = vCopy[ind[z]];

        cout<<"indices vector ordenado: "<<endl;
        for(int z=0;z<dist.size();z++)
            cout<<ind[z]<<endl;


        UAV_mission_points.push_back(p);
    }

    cout<<"*** UAV_mission_points ***"<<endl;
    for(int i=0;i<UAV_mission_points_v2.size();i++)
    {
        for(int j=0;j<UAV_mission_points_v2[i].size();j++)
            cout<<UAV_mission_points_v2[i][j]<<endl;
        cout<<endl<<endl;
    }

    cout<<"*** UAV_mission_points SORTED***"<<endl;
    for(int i=0;i<UAV_mission_points.size();i++)
    {
        for(int j=0;j<UAV_mission_points[i].size();j++)
            cout<<UAV_mission_points[i][j]<<endl;
        cout<<endl<<endl;
    }


    missions_points.clear();
    missions_points = sortMissionPoints(UAV_mission_points);



    cout<<"*** UAV_mission_points FINAL SORTED***"<<endl;
    for(int i=0;i<missions_points.size();i++)
    {
        for(int j=0;j<missions_points[i].size();j++)
            cout<<missions_points[i][j]<<endl;
        cout<<endl<<endl;
    }



    return missions_points;
}

std::vector<std::vector<cv::Point3f> >  droneGMPROSModule::sortMissionPoints(std::vector<std::vector<cv::Point3f> >& mission_points)
{
    std::vector<std::vector<cv::Point3f> > mission_points_sorted = mission_points;
    for(int i=0;i< mission_points_sorted.size();i++)
    {
        for(int j=0;j<mission_points_sorted[i].size()-1;j++)
        {
            std::vector<float> distances;
            for(int z=j;z<mission_points_sorted[i].size()-1;z++)
                distances.push_back(cv::norm(mission_points_sorted[i][j] - mission_points_sorted[i][z+1]));

            int new_idx = std::min_element(distances.begin(), distances.end()) - distances.begin() + 1 + j;
            std::iter_swap(mission_points_sorted[i].begin() + j + 1, mission_points_sorted[i].begin() + new_idx);

        }
    }

    return mission_points_sorted;
}

void droneGMPROSModule::sendMissionPointsToMissionPlanner()
{
    if(!moduleStarted)
        return;
    UAV_mission_points_msg.header.stamp = ros::Time::now();
    for(int i=0;i<missions_points.size();i++)
    {
        for(int j=0;j<missions_points[i].size();j++)
        {
            UAV_task_msg.x = missions_points[i][j].x;
            UAV_task_msg.y = missions_points[i][j].y;
            UAV_task_msg.z = missions_points[i][j].z;
            UAV_mission_points_msg.points3D.push_back(UAV_task_msg);
        }

        mission_points_publ.publish(UAV_mission_points_msg);
        if(i==0)
            break;
    }

}

std::vector<std::string>  droneGMPROSModule::getTaskModuleNames(std::string &task_name)
{
    std::vector<std::string> modules_names;

    if(task_name == "Takeoff")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back("droneSpeechROSModule");
                modules_names.push_back("droneSoundROSModule");
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                modules_names.push_back(MODULE_NAME_NAVIGATION_STACK);
                break;
        }
    }
    else if(task_name == "Hover")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
                modules_names.push_back(MODULE_NAME_ARUCO_EYE);
                modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
                modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
                modules_names.push_back(MODULE_NAME_LOCALIZER);
                modules_names.push_back("droneSpeechROSModule");
                modules_names.push_back("droneObstacleDistanceCalculator");
                modules_names.push_back("droneSoundROSModule");
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                modules_names.push_back(MODULE_NAME_NAVIGATION_STACK);
                break;
        }
    }
    else if(task_name == "MoveToPoint" || task_name == "TurnInYaw" || task_name == "Sleep" || task_name == "GoHome" || task_name == "Flip")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back(MODULE_NAME_TRAJECTORY_PLANNER);
                modules_names.push_back(MODULE_NAME_ARUCO_EYE);
                modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
                modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
                modules_names.push_back(MODULE_NAME_YAW_PLANNER);
                modules_names.push_back(MODULE_NAME_LOCALIZER);
                modules_names.push_back("droneSpeechROSModule");
                modules_names.push_back("droneObstacleDistanceCalculator");
                modules_names.push_back("droneSoundROSModule");
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                modules_names.push_back(MODULE_NAME_NAVIGATION_STACK);
                modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
                break;
        }
    }

    else if(task_name == "MoveInVisualServoing")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back(MODUlE_NAME_OPENTLD_TRANSLATOR);
                modules_names.push_back(MODULE_NAME_TRACKER_EYE);
                modules_names.push_back(MODULE_NAME_IBVS_CONTROLLER);
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                modules_names.push_back(MODULE_NAME_IBVS_CONTROLLER_FROM_TOP);
                //modules_names.push_back(MODULE_NAME_RL_IBVS_CONTROLLER_FROM_TOP);
                modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
                break;
        }
    }

    else if(task_name == "MoveInVisualServoingRl")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back(MODUlE_NAME_OPENTLD_TRANSLATOR);
                modules_names.push_back(MODULE_NAME_TRACKER_EYE);
                modules_names.push_back(MODULE_NAME_IBVS_CONTROLLER);
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                //modules_names.push_back(MODULE_NAME_IBVS_CONTROLLER_FROM_TOP);
                modules_names.push_back(MODULE_NAME_RL_IBVS_CONTROLLER_FROM_TOP);
                modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
                break;
        }
    }


    else if(task_name == "Land")
    {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
                    modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
                    modules_names.push_back("droneSpeechROSModule");
                    modules_names.push_back("droneSoundROSModule");
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                    modules_names.push_back(MODULE_NAME_NAVIGATION_STACK);
                    break;
            }
    }

    else if(task_name == "LandAutonomously")
    {
        switch(mission_type)
        {
            case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                modules_names.push_back(MODULE_NAME_ODOMETRY_STATE_ESTIMATOR);
                modules_names.push_back(MODULE_NAME_OBSTACLE_PROCESSOR);
                modules_names.push_back("droneSpeechROSModule");
                modules_names.push_back("droneSoundROSModule");
                break;
            case MissionType::EXPLORE_WITH_NAVIGATION:
                modules_names.push_back(MODULE_NAME_ROBOT_LOCALIZATION);
                modules_names.push_back(MODULE_NAME_TRAJECTORY_CONTROLLER);
                modules_names.push_back(MODULE_NAME_AUTONOMOUS_LANDING);
                break;
        }
    }



    return modules_names;
}

void droneGMPROSModule::generateMissionsTasksNames()
{
    switch(mission_type)
    {
        case MissionType::EXPLORE:
            for(int i=0;i<missions_points.size();i++)
            {
                std::vector<std::string> task_name;
                task_name.push_back("Takeoff");
                task_name.push_back("Hover");
                task_name.push_back("Land");
                missions_tasks_names.push_back(task_name);
                for(int j=0;j<missions_points[i].size();j++)
                {
                    missions_tasks_names[i].push_back("MoveToPoint");
                }
            }
            break;
        case MissionType::FIND_OBJECT:
            for(int i=0;i<missions_points.size();i++)
            {
                std::vector<std::string> task_name;
                task_name.push_back("Takeoff");
                task_name.push_back("Hover");
                task_name.push_back("Land");
                missions_tasks_names.push_back(task_name);
                for(int j=0;j<missions_points[i].size();j++)
                {
                    missions_tasks_names[i].push_back("MoveToPoint");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                    missions_tasks_names[i].push_back("TurnInYaw");
                    missions_tasks_names[i].push_back("Sleep");
                }
            }
            break;

        case MissionType::EXPLORE_WITH_NAVIGATION:
            for(int i=0;i<missions_points.size();i++)
            {
                std::vector<std::string> task_name;
                task_name.push_back("Takeoff");
                task_name.push_back("Hover");
                task_name.push_back("Land");
                //task_name.push_back("LandAutonomously");
                missions_tasks_names.push_back(task_name);
                for(int j=0;j<missions_points[i].size();j++)
                {
                    missions_tasks_names[i].push_back("MoveToPoint");
                    missions_tasks_names[i].push_back("Sleep");
//                    missions_tasks_names[i].push_back("TurnInYaw");
//                    missions_tasks_names[i].push_back("TurnInYaw");
//                    missions_tasks_names[i].push_back("TurnInYaw");
//                    missions_tasks_names[i].push_back("TurnInYaw");
                }
                missions_tasks_names[i].push_back("GoHome");
            }
            break;

    }

    for(int i=0;i<missions_tasks_names.size();i++)
    {
        for(int j=1;j<missions_tasks_names[i].size();j++)
        {
            if((missions_tasks_names[i][j-1] == "Land") || (missions_tasks_names[i][j-1] == "LandAutonomously"))
                std::iter_swap(missions_tasks_names[i].begin() + (j -1), missions_tasks_names[i].begin() + j);
        }
    }


    cout<<"*** Missions Names ***"<<endl;
    for(int i=0;i<missions_tasks_names.size();i++)
    {
        for(int j=0;j<missions_tasks_names[i].size();j++)
            cout<<missions_tasks_names[i][j]<<endl;
        cout<<endl<<endl;
    }
}




void droneGMPROSModule::generateBroadcastMissions()
{
    for(int i=0;i<missions_tasks_names.size();i++)
    {
        droneMsgsROS::droneMission drone_mission_msg;
        std::vector<cv::Point3f> UAV_mission_points;
        UAV_mission_points.insert(UAV_mission_points.end(), missions_points[i].begin(), missions_points[i].end());
        drone_mission_msg = generateMission(missions_tasks_names[i], UAV_mission_points, i);


        swarm_mission_list[i]->Add(drone_mission_msg);
    }
}


droneMsgsROS::droneMission droneGMPROSModule::generateMission(std::vector<std::string> &mission_tasks_names, std::vector<cv::Point3f> &UAV_mission_points, unsigned int UAV_id)
{

    droneMsgsROS::droneMission drone_mission_msg;
    drone_mission_msg.header.stamp = ros::Time::now();
    int move_to_point_counter = 0;
    int turn_in_yaw_counter = 0;
    for(unsigned int i=0;i<mission_tasks_names.size();i++)
    {
        if(turn_in_yaw_counter > 3)
            turn_in_yaw_counter = 0;
        droneMsgsROS::droneTask drone_task_msg;
        if(mission_tasks_names[i] == "Takeoff")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::TAKE_OFF;
            drone_task_msg.speech_name = "Despegando";
            //drone_task_msg.yaw = -500;
        }

        else if(mission_tasks_names[i] == "Hover")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::HOVER;
            drone_task_msg.speech_name = "Hovering";
            drone_task_msg.time = 1;
            //drone_task_msg.yaw = -500;
        }

        else if(mission_tasks_names[i] == "MoveToPoint")
        {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                    drone_task_msg.speech_name = "Movimiento en trayectoria";
                    drone_task_msg.yawSelector = 2;
                    drone_task_msg.point.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.point.y = UAV_mission_points[move_to_point_counter].y;
                    drone_task_msg.point.z = UAV_mission_points[move_to_point_counter].z;
                    drone_task_msg.pointToLook.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.pointToLook.y = UAV_mission_points[move_to_point_counter].y;
                    //drone_task_msg.yaw = -500;
                    move_to_point_counter++;
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_POSITION;
                    drone_task_msg.speech_name = "Movimiento en Posicion con Navigation Stack";
                    drone_task_msg.yawSelector = 0;
                    drone_task_msg.point.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.point.y = UAV_mission_points[move_to_point_counter].y;
                    drone_task_msg.point.z = UAV_mission_points[move_to_point_counter].z;
                    move_to_point_counter++;
                    break;
            }

        }

        else if(mission_tasks_names[i] == "TurnInYaw")
        {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                    drone_task_msg.speech_name = "Girando en yaw";
                    drone_task_msg.yawSelector = 1;
                    switch(turn_in_yaw_counter)
                    {
                        case 0:
                            drone_task_msg.yaw = 90;
                            break;
                        case 1:
                            drone_task_msg.yaw = 180;
                            break;
                        case 2:
                            drone_task_msg.yaw = 0;
                            break;
                        case 3:
                            drone_task_msg.yaw = 90;
                            break;
                    }
                    turn_in_yaw_counter++;
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_POSITION;
                    drone_task_msg.speech_name = "Girando en yaw";
                    drone_task_msg.yawSelector = 1;
                    switch(turn_in_yaw_counter)
                    {
                        case 0:
                            drone_task_msg.yaw = 90;
                            break;
                        case 1:
                            drone_task_msg.yaw = 180;
                            break;
                        case 2:
                            drone_task_msg.yaw = 270;
                            break;
                        case 3:
                            drone_task_msg.yaw = 0;
                            break;
                    }
                    turn_in_yaw_counter++;
                    break;


            }
        }

        else if(mission_tasks_names[i] == "Sleep")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::SLEEP;
            drone_task_msg.speech_name = "Sleeping";
            drone_task_msg.time = 1;
            //drone_task_msg.yaw = -500;
        }

        else if(mission_tasks_names[i] == "Land")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::LAND;
            drone_task_msg.speech_name = "Aterrizar";
            //drone_task_msg.yaw = -500;

        }

        else if(mission_tasks_names[i] == "LandAutonomously")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::LAND_AUTONOMOUS;
            drone_task_msg.speech_name = "Autonomous Landing";
        }

        else if(mission_tasks_names[i] == "GoHome")
        {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_TRAJECTORY;
                    drone_task_msg.speech_name = "Retorno al punto inicial";
                    drone_task_msg.point.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.point.y = UAV_mission_points[move_to_point_counter].y;
                    drone_task_msg.point.z = UAV_mission_points[move_to_point_counter].z;
                    drone_task_msg.yawSelector = 2;
                    drone_task_msg.pointToLook.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.pointToLook.y = UAV_mission_points[move_to_point_counter].y;
                    move_to_point_counter++;
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_POSITION;
                    drone_task_msg.speech_name = "Retorno al punto inicial";
                    drone_task_msg.point.x = takeoff_UAVs_points[UAV_id].x;
                    drone_task_msg.point.y = takeoff_UAVs_points[UAV_id].y;
                    drone_task_msg.point.z = takeoff_UAVs_points[UAV_id].z;
                    drone_task_msg.yawSelector = 2;
                    drone_task_msg.pointToLook.x = UAV_mission_points[move_to_point_counter].x;
                    drone_task_msg.pointToLook.y = UAV_mission_points[move_to_point_counter].y;
                    move_to_point_counter++;
                    break;
            }
        }

        else if(mission_tasks_names[i] == "MoveInVisualServoing")
        {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING;
                    drone_task_msg.speech_name = "Moving Visual Servoing";
                    drone_task_msg.time = 40;
                    break;
            }
        }

        else if(mission_tasks_names[i] == "MoveInVisualServoingRl")
        {
            switch(mission_type)
            {
                case (MissionType::EXPLORE || MissionType::FIND_OBJECT):
                    break;
                case MissionType::EXPLORE_WITH_NAVIGATION:
                    drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_VISUAL_SERVOING_RL;
                    drone_task_msg.speech_name = "Moving Visual Servoing with Reinforcement Learning";
                    drone_task_msg.time = 500;
                    break;
            }
        }

        else if(mission_tasks_names[i] == "Flip")
        {
            drone_task_msg.mpCommand = droneMsgsROS::droneTask::MOVE_FLIP_RIGHT;
            drone_task_msg.speech_name = "Flip";
        }


        drone_task_msg.module_names = getTaskModuleNames(mission_tasks_names[i]);
        drone_mission_msg.mission.push_back(drone_task_msg);


    }
    return drone_mission_msg;

}


void droneGMPROSModule::sendBroadcastAreaInfoToSubmissionPlanner()
{
    for(int i=0;i<num_UAVs;i++)
    {
        //For publishing the Area Info to each UAV of the Swarm
        swarmGMP_area_info_publ[i].publish(area_info_msg);
    }
}


void droneGMPROSModule::sendBroadcastMissionToSubmissionPlanner()
{
    if(!moduleStarted)
        return;
    for(int i=0;i<num_UAVs;i++)
    {
        //For publishing the Mission to each UAV of the Swarm
        swarmGMP_missions_publ[i].publish(swarm_mission_list[i]->getCurrentMission());
    }
}


void droneGMPROSModule::sendMissionToSubmissionPlanner(droneMsgsROS::droneMission mission, unsigned int UAV_id)
{
    if(!moduleStarted)
        return;
    //cout<<"++++** Mission To AMP  **++++"<<endl<<mission<<endl<<endl;
    swarmGMP_missions_publ[UAV_id].publish(mission);
}


void droneGMPROSModule::sendBroadcastBreakMissionToSubmissionPlanner()
{
    if(!moduleStarted)
        return;
    std_msgs::Bool break_mission_msg;
    break_mission_msg.data = true;
    for(int i=0;i<num_UAVs;i++)
    {
        //For publishing the Mission to each UAV of the Swarm
        swarmGMP_break_mission_publ[i].publish(break_mission_msg);
    }
}


void droneGMPROSModule::sendBreakMissionToSubmissionPlanner(unsigned int UAV_id)
{
    if(!moduleStarted)
        return;
    std_msgs::Bool break_mission_msg;
    break_mission_msg.data = true;
    swarmGMP_break_mission_publ[UAV_id].publish(break_mission_msg);
}










