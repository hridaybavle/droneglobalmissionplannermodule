#include "ros/ros.h"
#include "droneGMPModule.h"
#include "nodes_definition.h"
using namespace std;

int main (int argc,char **argv)
{
    //ROS init
    ros::init(argc, argv, "droneGlobalMissionPlanner");
    ros::NodeHandle n;
    std::cout << "[ROSNODE] Starting" << "droneGlobalMissionPlannerROSModule" << std::endl;

    droneGMPROSModule MyGMPROSModule;
    MyGMPROSModule.open(n, "droneGlobalMissionPlanner");
    MyGMPROSModule.init();


    try
    {
        while(ros::ok())
        {
            ros::spin();
        }

        return 1;
    }
    catch (std::exception &ex)
    {
        std::cout<<"[ROSNODE] Exception :"<<ex.what()<<std::endl;
    }

}
